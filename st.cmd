require essioc
require caenelsmagnetps, 1.2.2+1
require magnetps, 1.1.0

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet("IOCNAME", "MEBT-010:SC-IOC-007")
epicsEnvSet("IOCDIR", "MEBT-010_SC-IOC-007")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# MEBT Magnets IP addresses
############################################################################
iocshLoad("$(E3_CMD_TOP)/ngps_ips.iocsh")
#
#
############################################################################
# Load conversion break point table
############################################################################
dbLoadDatabase("MEBT-010_BMD-QH-011.dbd", "$(magnetps_DB)/", "")
updateMenuConvert
#
#
############################################################################
# MEBT Quadrupole 11 -> QH-11 
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSQH-011")
epicsEnvSet(PSPORT, "Q-11")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS$(PSPORT), IP_ADDR=$(PSQH11_IP), P=MEBT-010:, R=$(PSUNIT):")
afterInit('seq program_snl "P=MEBT-010:, R=PwrC-PSQH-011:Wave-"')
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-QH-011")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T/m, ADEL=0")
#
#
#-###########################################################################
#- MEBT Quadrupole SPARE
#-###########################################################################
#- epicsEnvSet(PSUNIT, "PwrC-PSQ-013")
#- epicsEnvSet(PSPORT, "Q-12")
#- iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS$(PSPORT), IP_ADDR=$(SPARE_IP), P=MEBT-010:, R=$(PSUNIT):")
#- afterInit('seq program_snl "P=MEBT-010:, R=PwrC-PSQ-012:Wave-"')
#-
#- epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-QH-011")
#- dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T/m, ADEL=0")
#
#
iocInit()
